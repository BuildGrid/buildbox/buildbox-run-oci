set(CMAKE_CXX_STANDARD 17)

find_file(BuildboxGTestSetup.cmake BuildboxGTestSetup.cmake HINTS ${BuildboxCommon_DIR})
include(${BuildboxGTestSetup.cmake})

set(target ocirunner_tests)

add_executable(${target}
  oci.t.cpp
  run_subprocess.t.cpp
  ../buildboxrun-oci/buildboxrun_oci.cpp
  ../buildboxrun-oci/buildboxrun_run_subprocess.cpp
  ../buildboxrun-oci/buildboxrun_container_host.cpp
)

target_include_directories(${target} PRIVATE ../buildboxrun-oci)
target_link_libraries(${target} Buildbox::buildboxcommon nlohmann_json::nlohmann_json ${GTEST_MAIN_TARGET} ${GTEST_TARGET} ${GMOCK_TARGET})
add_test(NAME ${target}
         COMMAND ${target})
