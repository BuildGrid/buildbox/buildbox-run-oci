# grpc

This directory contains a single gRPC public header file that
doesn't get installed when gRPC is compiled with CMake without
`-DgRPC_BUILD_TESTS=ON`. Upstream issue is tracked at
https://github.com/grpc/grpc/issues/20208

The known packages without this header are in Homebrew and OpenSUSE.

Homebrew issue is tracked at
https://github.com/Homebrew/homebrew-core/issues/165487

Remove this directory once upstream or Homebrew have fixed it and it
has been propagated to all distributions we care about.
