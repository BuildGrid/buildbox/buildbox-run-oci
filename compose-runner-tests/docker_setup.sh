#!/usr/bin/env bash
set -eux -o pipefail

docker-compose -f docker-compose-runner.yaml build
trap "docker-compose -f docker-compose-runner.yaml down" EXIT
docker-compose -f docker-compose-runner.yaml up -d && sleep 10
docker-compose -f docker-compose-runner.yaml exec -T runner compose-runner-tests/runner.sh