FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest

RUN apt-get update && apt-get install -y docker.io

COPY . /buildbox-run-oci

RUN cd /buildbox-run-oci \
    && mkdir build && cd build && cmake .. && make

