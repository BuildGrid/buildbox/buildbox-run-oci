#!/usr/bin/env python3
import argparse

import buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 as reapi
from buildgrid.settings import HASH


def main():
    parser = argparse.ArgumentParser("action-creator")
    parser.add_argument("--arguments", required=True)
    parser.add_argument("--input-root-digest")
    parser.add_argument("--image-name", required=True)
    parser.add_argument("--working-directory", default="")
    parser.add_argument("--command-file", default="command.data")
    parser.add_argument("--action-file", default="action.data")
    parser.add_argument("--output-digest-file", default="action.digest")
    args = parser.parse_args()

    command = reapi.Command(
        arguments=args.arguments.split(" "),
        working_directory=args.working_directory,
        # deprecated platform
        platform=reapi.Platform(
            properties=[
                reapi.Platform.Property(
                    name="container-image", value=args.image_name
                )
            ]
        ),
    )

    if args.input_root_digest:
        input_root_digest_hash, input_root_digest_size  = args.input_root_digest.split("/")
        input_root_digest = reapi.Digest(hash=input_root_digest_hash, size_bytes=int(input_root_digest_size))
    else:
        input_root_digest = None

    command_data = command.SerializeToString()
    command_hash = HASH()
    command_hash.update(command_data)
    command_digest = reapi.Digest(hash=command_hash.hexdigest(), size_bytes=len(command_data))

    action = reapi.Action(
        command_digest=command_digest,
        input_root_digest=input_root_digest,
        platform=reapi.Platform(
            properties=[
                reapi.Platform.Property(
                    name="container-image", value=args.image_name
                )
            ]
        ),
    )
    action_data = action.SerializeToString()
    action_hash = HASH()
    action_hash.update(action_data)
    action_digest = reapi.Digest(hash=action_hash.hexdigest(), size_bytes=len(action_data))

    with open(args.command_file, "wb") as f:
        f.write(command_data)

    with open(args.action_file, "wb") as f:
        f.write(action_data)

    with open(args.output_digest_file, "w") as f:
        f.write(f"{action_digest.hash}/{action_digest.size_bytes}")

    print(args.output_digest_file, f"{action_digest.hash}/{action_digest.size_bytes}")


if __name__ == "__main__":
    main()