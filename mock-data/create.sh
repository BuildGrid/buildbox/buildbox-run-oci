cd "$(dirname "$0")"
docker run -v `pwd`:/data:z -t registry.gitlab.com/buildgrid/buildbox/buildbox-e2e/buildbox-e2e-base:latest casupload \
  --dry-run --output-digest-file=/data/outputs/inputs.digest /data/inputs

docker build -t create-action .
docker run --entrypoint create-action -v `pwd`:/data:z -t create-action \
  --arguments '/bin/ls /' \
  --image-name frolvlad/alpine-gxx@sha256:a5f206ffb5b4ef2d66dd04128b62c9b3decefb4cdb1931bdfc0f4b987e9a558f \
  --input-root-digest $(cat outputs/inputs.digest) \
  --command-file /data/outputs/command.data \
  --action-file /data/outputs/action.data \
  --output-digest-file /data/outputs/action.digest
